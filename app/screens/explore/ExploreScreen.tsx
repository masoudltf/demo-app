import React from "react";
import {View, StyleSheet, ImageBackground, Dimensions, Image, Text, TouchableOpacity, StatusBar, Animated} from "react-native";
import {useIsFocused} from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';
import {FlatList} from "react-native-gesture-handler";

const LOREM_IPSUM = 'In publishing and graphic design, Lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document or a typeface without relying on meaningful content. Lorem ipsum may be used as a placeholder before final copy is available.';
const SAMPLE_DATA = [
    {
        username: 'Robert Lewandowski',
        location: 'Muchen, Germany',
        image: require('../../../assets/images/sample1.jpg'),
        tUp: 12,
        tDown: 0,
        like: 12,
        comment: 6,
        share: 8
    },
    {
        username: 'Cristiano Ronaldo',
        location: 'Turin, Italy',
        image: require('../../../assets/images/sample2.jpg'),
        tUp: 5,
        tDown: 1,
        like: 4,
        comment: 2,
        share: 5
    },
    {
        username: 'Lionel Messi',
        location: 'Barcelona, Spain',
        image: require('../../../assets/images/sample3.jpg'),
        tUp: 6,
        tDown: 2,
        like: 5,
        comment: 1,
        share: 2
    }
];

const SAMPLE_DATA_HEADER = [
    {
        title: 'Costco',
        members: 20,
        active: true
    },
    {
        title: 'Target',
        members: 12
    },
    {
        title: 'Trader',
        members: 15
    },
    {
        title: 'Worker',
        members: 25
    },
    {
        title: 'Friends',
        members: 8
    },
    {
        title: 'School',
        members: 6
    }
];

export const ExploreScreen = (props) => {

    let renderItem = ({item}) => {

        let animValue = new Animated.Value(1);
        let isVisible = true;
        let toggleOpacity = () => {
            Animated.timing(animValue, {
                toValue: isVisible ? 0 : 1,
                useNativeDriver: false
            }).start();
            isVisible = !isVisible;
        }

        return (
            <ImageBackground source={item.image}
                             resizeMode={'cover'}
                             style={styles.itemContainer}>
                <TouchableOpacity style={{flex: 1}}
                                  activeOpacity={1}
                                  onPress={toggleOpacity}>
                    <Animated.View style={{flex: 1, opacity: animValue}}>
                        <View style={styles.layoutButtons}>
                            <Image source={require('../../../assets/images/exploreIcons/bag.png')}
                                   style={styles.layoutButton}
                                   resizeMode={'center'} />
                            <Image source={require('../../../assets/images/exploreIcons/more.png')}
                                   style={styles.layoutButton}
                                   resizeMode={'center'} />
                        </View>
                        <LinearGradient colors={['#00000000', '#000000']} style={styles.itemGradient}>
                            <View style={styles.itemHeader}>
                                <View style={styles.itemUserImageContainer}>
                                    <Image source={require('../../../assets/images/tabIcons/account.png')}
                                           resizeMode={'center'}
                                           style={styles.itemUserImage}/>
                                </View>
                                <View style={styles.itemUsername}>
                                    <Text style={styles.itemUsernameTitle}>{item.username}</Text>
                                    <Text style={styles.itemUsernameDesc}>{'at ' + item.location}</Text>
                                </View>
                                <TouchableOpacity style={styles.itemDate}>
                                    <Text style={styles.itemDateText}>{'* 25m'}</Text>
                                </TouchableOpacity>
                            </View>
                            <Text style={styles.itemDesc} numberOfLines={3}>
                                {LOREM_IPSUM}
                            </Text>
                            <View style={styles.itemBottom}>
                                <TouchableOpacity style={styles.itemBottomButton}>
                                    <Image source={require('../../../assets/images/exploreIcons/tUp.png')}
                                           style={styles.itemBottomIcon}
                                           resizeMode={'center'} />
                                    <Text style={styles.itemBottomText}>{item.tUp}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.itemBottomButton}>
                                    <Image source={require('../../../assets/images/exploreIcons/tDown.png')}
                                           style={styles.itemBottomIcon}
                                           resizeMode={'center'} />
                                    <Text style={styles.itemBottomText}>{item.tDown}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.itemBottomButton}>
                                    <Image source={require('../../../assets/images/exploreIcons/like.png')}
                                           style={styles.itemBottomIcon}
                                           resizeMode={'center'} />
                                    <Text style={styles.itemBottomText}>{item.like}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.itemBottomButton}>
                                    <Image source={require('../../../assets/images/exploreIcons/comment.png')}
                                           style={styles.itemBottomIcon}
                                           resizeMode={'center'} />
                                    <Text style={styles.itemBottomText}>{item.comment}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.itemBottomButton}>
                                    <Image source={require('../../../assets/images/exploreIcons/share.png')}
                                           style={styles.itemBottomIcon}
                                           resizeMode={'center'} />
                                    <Text style={styles.itemBottomText}>{item.share}</Text>
                                </TouchableOpacity>
                            </View>
                        </LinearGradient>
                    </Animated.View>
                </TouchableOpacity>
            </ImageBackground>
        )
    }

    return (
        <View style={{flex: 1}}>
            {
                useIsFocused()
                    ?
                    <StatusBar backgroundColor={'transparent'}
                               barStyle={'light-content'}
                               animated={true}
                               hidden={false}
                               translucent={true}/>
                    :
                    null
            }
            <LinearGradient colors={['#000000', '#00000000']}
                            style={styles.header}>
                <FlatList data={SAMPLE_DATA_HEADER}
                          horizontal={true}
                          contentContainerStyle={styles.headerTab}
                          showsHorizontalScrollIndicator={false}
                          renderItem={({item}) => {
                              return (
                                  <View style={{alignItems: 'center', justifyContent: 'flex-start'}}>
                                      <Text style={styles.headerTabText(item.active)}
                                            onPress={() => alert(item.title)}>
                                          {item.title}
                                      </Text>
                                      {item.active ? <Text style={{
                                          fontSize: 12,
                                          color: '#ffffffbb'
                                      }}>{item.members + ' members'}</Text> : null}
                                  </View>
                              )
                          }}/>
            </LinearGradient>
            <FlatList data={SAMPLE_DATA}
                      pagingEnabled={true}
                      horizontal={true}
                      renderItem={renderItem} />
        </View>
    );
}

const accentColor = '#fc5e36';

const styles = StyleSheet.create({
    header: {
        position: 'absolute',
        top: 0,
        width: '100%',
        left: 0,
        height: 150,
        maxHeight: 150,
        zIndex: 1
    },

    headerTab: {
        paddingStart: 150,
        marginTop: 50,
        justifyContent: 'flex-end'
    },

    headerTabText: (isActive) => {
        return {
            color: isActive ? '#ffffff' : '#ddddddaa',
            fontWeight: isActive ? 'bold' : 'normal',
            fontSize: isActive ? 20 : 18,
            marginHorizontal: 20,
            marginBottom: 5
        }
    },

    itemContainer: {
        width: Dimensions.get('window').width,
        flex: 1,
        paddingTop: 150,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },

    itemGradient: {
        paddingHorizontal: 15,
        paddingTop: 50,
        paddingBottom: 50,
        minHeight: 200,
        width: '100%',
        position: 'absolute',
        bottom: 0
    },

    itemHeader: {
        flexDirection: 'row',
        alignItems: 'center'
    },

    itemUserImageContainer: {
        backgroundColor: '#eeeeee',
        width: 50,
        height: 50,
        borderRadius: 25,
        borderWidth: 2,
        borderColor: '#dddddd'
    },

    itemUserImage: {
        tintColor: '#333333',
        width: '100%',
        height: '100%'
    },

    itemUsername: {
        marginStart: 10,
        flex: 1
    },

    itemUsernameTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#ffffff'
    },

    itemUsernameDesc: {
        fontSize: 15,
        color: '#cccccc'
    },

    itemDate: {
        backgroundColor: accentColor,
        paddingHorizontal: 5,
        paddingVertical: 2,
        borderRadius: 10,
        position: 'absolute',
        end: 15,
        bottom: 0
    },

    itemDateText: {
        fontSize: 12,
        color: '#ffffff'
    },

    itemDesc: {
        paddingVertical: 14,
        fontSize: 15,
        color: '#cccccc',
        lineHeight: 20
    },

    itemBottom: {
        flexDirection: 'row-reverse',
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 10,
        paddingBottom: 25,
        borderTopColor: '#ffffff55',
        borderTopWidth: 1
    },

    itemBottomButton: {
        flex: 1,
        flexDirection: 'row',
        paddingVertical: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },

    itemBottomIcon: {
        marginEnd: 5,
        width: 20,
        height: 20,
        tintColor: '#dddddddd'
    },

    itemBottomText: {
        fontSize: 12,
        color: '#dddddddd'
    },

    layoutButtons: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        width: '100%',
        paddingEnd: 10
    },

    layoutButton: {
        tintColor: '#ffffff',
        width: 30,
        height: 30,
        marginHorizontal: 10
    }

})
