import React from "react";
import {View, Text, StyleSheet, Image, FlatList, TouchableOpacity} from "react-native";
import {Screen} from "../../components";
import {ScrollView} from "react-native-gesture-handler";

const SAMPLE_RECENT_ACTIVITY = [
    {
        image: 'https://picsum.photos/id/0/150/150'
    },
    {
        image: 'https://picsum.photos/id/10/150/150'
    },
    {
        image: 'https://picsum.photos/id/20/150/150'
    },
    {
        image: 'https://picsum.photos/id/30/150/150'
    },
    {
        image: 'https://picsum.photos/id/40/150/150'
    },
    {
        image: 'https://picsum.photos/id/50/150/150'
    },
    {
        image: 'https://picsum.photos/id/60/150/150'
    },
    {
        image: 'https://picsum.photos/id/70/150/150'
    },
    {
        image: 'https://picsum.photos/id/80/150/150'
    }
]

const SAMPLE_YOU_HELPED = [
    {
        name: 'Saul',
        image: 'https://picsum.photos/id/90/150/150'
    },
    {
        name: 'Tom',
        image: 'https://picsum.photos/id/100/150/150'
    },
    {
        name: 'Sara',
        image: 'https://picsum.photos/id/110/150/150'
    },
    {
        name: 'Maria',
        image: 'https://picsum.photos/id/120/150/150'
    },
    {
        name: 'Jojo',
        image: 'https://picsum.photos/id/130/150/150'
    }
];

const SAMPLE_HELPED_YOU = [
    {
        name: 'Sara',
        image: 'https://picsum.photos/id/140/150/150'
    },
    {
        name: 'Jane',
        image: 'https://picsum.photos/id/151/150/150'
    },
    {
        name: 'Rick',
        image: 'https://picsum.photos/id/160/150/150'
    },
    {
        name: 'Marty',
        image: 'https://picsum.photos/id/170/150/150'
    },
    {
        name: 'Melina',
        image: 'https://picsum.photos/id/180/150/150'
    },
    {
        name: 'Nicole',
        image: 'https://picsum.photos/id/190/150/150'
    },
];

export class AccountScreen extends React.Component {
    render() {
        return (
            <Screen statusBar={'dark-content'}
                    style={styles.container}>
                <View style={styles.userImage}>
                    <Image source={require('../../../assets/images/me.jpg')}
                           resizeMode={'center'}
                           style={{width: '100%', height: '100%', borderRadius: 150}}  />
                    <View style={styles.imageBadge}>
                        <Text style={{color: '#ffffff', fontSize: 12}}>{'❤'}</Text>
                        <Text style={{color: '#ffffff', fontSize: 10}}>{'20'}</Text>
                    </View>

                </View>
                <View style={{alignItems: 'center'}}>
                    <Text style={{fontWeight: 'bold', marginTop: 10, fontSize: 20}}>{'Christian Bale'}</Text>
                    <Text style={{color: '#777777', marginBottom: 10, fontSize: 13}}>{'Los Angeles, California'}</Text>
                    <View style={{flexDirection: 'row', marginTop: 10}}>
                        <Text style={{color: '#777777', marginTop: 10, fontSize: 15}}>{'Added by '}</Text>
                        <Text style={{fontWeight: 'bold', marginTop: 10, fontSize: 15}}>{'Brad Pitt'}</Text>
                        <Text style={{color: '#777777', marginTop: 10, fontSize: 15}}>{', Joined Yesterday'}</Text>
                    </View>
                </View>

                <ScrollView style={{flex: 1}}>
                    <View style={styles.section}>
                        <Text style={styles.sectionText}>{'Recent activity'}</Text>
                        <FlatList data={SAMPLE_RECENT_ACTIVITY}
                                  horizontal={true}
                                  showsHorizontalScrollIndicator={false}
                                  style={{marginTop: 15}}
                                  renderItem={({item}) => {
                                      return (
                                          <View style={styles.round}>
                                              <Image source={{uri: item.image}} resizeMode={'cover'} style={styles.listImage} />
                                          </View>
                                      )
                                  }} />
                    </View>
                    <View style={styles.section}>
                        {getYouHelped()}
                        <FlatList data={SAMPLE_YOU_HELPED}
                                  horizontal={true}
                                  showsHorizontalScrollIndicator={false}
                                  style={{marginTop: 15}}
                                  renderItem={({item}) => {
                                      return (
                                          <View style={styles.circle}>
                                              <Image source={{uri: item.image}} resizeMode={'cover'} style={styles.listImage} />
                                          </View>
                                      )
                                  }} />
                    </View>
                    <View style={styles.section}>
                        {getHelpedYou()}
                        <FlatList data={SAMPLE_HELPED_YOU}
                                  horizontal={true}
                                  showsHorizontalScrollIndicator={false}
                                  style={{marginTop: 15}}
                                  renderItem={({item}) => {
                                      return (
                                          <View style={styles.circle}>
                                              <Image source={{uri: item.image}} resizeMode={'cover'} style={styles.listImage} />
                                          </View>
                                      )
                                  }} />
                    </View>
                </ScrollView>

                <View style={styles.footer}>
                    <TouchableOpacity style={styles.addItem}
                                      onPress={() => alert('Invite Friends')}>
                        <Text style={styles.addItemText}>{'Invite Friends'}</Text>
                    </TouchableOpacity>
                    <Text style={styles.footerText}>{'5 out of 8 friends have joined'}</Text>
                </View>

            </Screen>
        );
    }
}

function getYouHelped() {
    let prefix = <Text style={styles.sectionText}>{'You recently helped '}</Text>;
    let and = <Text style={styles.sectionText}>{' and '}</Text>;
    let others = <Text style={styles.sectionTextBold}>{(SAMPLE_YOU_HELPED.length - 3) + ' others:'}</Text>;
    let last = null;
    let canLast = SAMPLE_YOU_HELPED.length > 1;
    let canOthers = SAMPLE_YOU_HELPED.length > 4;
    let maxName = (SAMPLE_YOU_HELPED.length - 1 > 3 ? 3 : (SAMPLE_YOU_HELPED.length === 1 ? 1 : SAMPLE_YOU_HELPED.length - 1));
    if (canLast) {
        last = <Text style={styles.sectionTextBold}>{SAMPLE_YOU_HELPED[SAMPLE_YOU_HELPED.length - 1].name}</Text>
    }

    let helperNumber = canOthers ? SAMPLE_YOU_HELPED.length - 2 : (canLast ? 2 : 1);

    return (
        <View style={styles.sectionTextComplex}>
            {prefix}
            {
                SAMPLE_YOU_HELPED.slice(0, maxName).map((item, index) => {
                    let comma = index === SAMPLE_YOU_HELPED.length - helperNumber ? '' : ', ';
                    return <Text style={styles.sectionTextBold}>{item.name + comma}</Text>
                })
            }
            {
                canLast ? and : null
            }
            {
                canOthers ? others : last
            }
        </View>
    )
}

function getHelpedYou() {
    let postfix = <Text style={styles.sectionText}>{' helped you recently:'}</Text>;
    let and = <Text style={styles.sectionText}>{' and '}</Text>;
    let others = <Text style={styles.sectionTextBold}>{(SAMPLE_HELPED_YOU.length - 3) + ' others'}</Text>;
    let last = null;
    let canLast = SAMPLE_HELPED_YOU.length > 1;
    let canOthers = SAMPLE_HELPED_YOU.length > 4;
    let maxName = (SAMPLE_HELPED_YOU.length - 1 > 3 ? 3 : (SAMPLE_HELPED_YOU.length === 1 ? 1 : SAMPLE_HELPED_YOU.length - 1));
    if (canLast) {
        last = <Text style={styles.sectionTextBold}>{SAMPLE_HELPED_YOU[SAMPLE_HELPED_YOU.length - 1].name}</Text>
    }

    let helperNumber = canOthers ? SAMPLE_HELPED_YOU.length - 2 : (canLast ? 2 : 1);

    return (
        <View style={styles.sectionTextComplex}>
            {
                SAMPLE_HELPED_YOU.slice(0, maxName).map((item, index) => {
                    let comma = index === SAMPLE_HELPED_YOU.length - helperNumber ? '' : ', ';
                    return <Text style={styles.sectionTextBold}>{item.name + comma}</Text>
                })
            }
            {
                canLast ? and : null
            }
            {
                canOthers ? others : last
            }
            {postfix}
        </View>
    )
}

const accentColor = '#fc5e36';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#eeeeee',
        paddingBottom: 50
    },

    userImage: {
        width: 100,
        height: 100,
        marginTop: 25
    },

    imageBadge: {
        position: 'absolute',
        backgroundColor: accentColor,
        width: 40,
        height: 40,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        bottom: -5,
        end: -15
    },

    section: {
        alignItems: 'flex-start',
        width: '100%',
        paddingHorizontal: 15,
        marginVertical: 20
    },

    sectionTextComplex: {
        flexDirection: 'row'
    },

    sectionText: {
        color: '#999999'
    },

    sectionTextBold: {
        fontWeight: 'bold',
        color: '#444444'
    },

    circle: {
        width: 40,
        height: 40,
        borderRadius: 25,
        marginEnd: 5,
        backgroundColor: '#aaaaaa',
        overflow: 'hidden'
    },

    round: {
        width: 70,
        height: 70,
        borderRadius: 20,
        marginEnd: 5,
        backgroundColor: '#aaaaaa',
        overflow: 'hidden'
    },

    listImage: {
        width: '100%',
        height: '100%'
    },

    footer: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 150
    },

    footerText: {
        color: '#999999',
        marginTop: 15
    },

    addItem: {
        backgroundColor: accentColor,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        width: 200,
        height: 40
    },

    addItemText: {
        color: '#ffffff',
        fontWeight: 'bold'
    }
})
