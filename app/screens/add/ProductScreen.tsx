import React from "react";
import {View, Text, StyleSheet, Image, ActivityIndicator} from "react-native";
import {Api} from "../../services/api";
import {Screen} from "../../components";

export class ProductScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            product: undefined,
            loading: true
        }
    }

    componentDidMount() {
        this.getProduct()
    }

    private getProduct = () => {
        let {productId} = this.props.route.params;
        let api = new Api();
        api.getProduct(productId)
            .then(response => {
                this.setState({
                    product: response.product,
                    loading: false
                })
            })
            .catch(error => {})
    }

    render() {
        let {
            product,
            loading
        } = this.state;

        return (
            <Screen statusBar={'dark-content'} style={styles.container}>
                {
                    loading
                        ?
                        <ActivityIndicator animating={true} />
                        :
                        <>
                            <Image source={{uri: product.image}} resizeMode={'cover'} style={styles.image} />
                            <Text style={styles.name}>{product.name}</Text>
                            <Text style={styles.price}>{'Price: ' + product.price + ' USD'}</Text>
                        </>
                }
            </Screen>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: '#ffffff'
    },

    image: {
        width: '80%',
        height: 200,
        borderRadius: 15
    },

    name: {
        fontSize: 15,
        fontWeight: 'bold',
        marginVertical: 10
    },

    price: {

    }

})
