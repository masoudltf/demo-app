import React from "react";
import {View, Text, StyleSheet, FlatList, TouchableOpacity, Image} from "react-native";
import {ShoppingList} from "../../models";
import {Api} from "../../services/api";
import {Button, Screen} from "../../components";

export class ProductsScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            products: []
        }
    }

    componentDidMount() {
        this.getProducts();
    }

    private getProducts = () => {
        const api = new Api();
        api.getProducts()
            .then(response => {
                this.setState({
                    products: response.products
                })
            })
            .catch(error => {})
    }

    private renderItem = ({item}) => {
        let {navigation} = this.props;

        return (
            <View style={styles.listItem}>
                <Text style={{width: 25}}>{item.id}</Text>
                <Text onPress={() => navigation.navigate('ProductScreen', {productId: item.id})}
                      style={{flex: 1, marginEnd: 5, color: '#0083de'}}
                      numberOfLines={1}>{item.name}</Text>
                <Text style={{width: 50}}>{item.price + ' USD'}</Text>
                <Image source={{uri: item.image}} style={styles.image} resizeMode={'cover'} />
                <TouchableOpacity style={styles.addButton} onPress={() => ShoppingList.addItem(item)}>
                    <Text style={styles.addButtonText}>{'+ Add'}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    render() {
        let {products} = this.state;
        return (
            <Screen statusBar={'dark-content'} style={styles.container}>
                <View style={styles.header}>
                    <Text style={{width: 25}}>{'ID'}</Text>
                    <Text style={{flex: 1}} numberOfLines={1}>{'Name'}</Text>
                    <Text style={{width: 50}}>{'Price'}</Text>
                    <Text style={{width: 60}}>{'Preview'}</Text>
                    <Text style={{width: 50}}>{'Action'}</Text>
                </View>
                <FlatList style={styles.list}
                          data={products}
                          renderItem={this.renderItem} />
            </Screen>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff'
    },

    list: {
        flex: 1,
        width: '100%',
        paddingHorizontal: 15
    },

    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 10,
        marginHorizontal: 15,
        borderBottomWidth: 2,
        borderBottomColor: '#cccccc'
    },

    listItem: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#eeeeee'
    },

    addButton: {
        width: 50,
        // paddingHorizontal: 10,
        paddingVertical: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        backgroundColor: '#22b670',
    },

    addButtonText: {
        fontSize: 12,
        color: '#ffffff'
    },

    image: {
        width: 50,
        height: 50,
        marginHorizontal: 5,
        borderRadius: 10
    }
})
