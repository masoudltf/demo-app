import React from "react";
import {View, Text, StyleSheet, FlatList, TouchableOpacity, Image, StatusBar} from "react-native";
import {Screen} from "../../components";
import {ShoppingList} from "../../models";
import {observer} from "mobx-react-lite";
import _ from 'lodash';

export const ShoppingListScreen = observer(({navigation}) => {

    let addItem = () => {
        navigation.navigate('ProductsScreen')
    }

    let renderItem = ({item}) => {
        const {items} = ShoppingList;
        let counts = _.countBy(items, 'id');

        return (
            <View style={styles.itemRow}>
                <Text style={{flex: 1, paddingEnd: 5}} numberOfLines={1}>{item.name}</Text>
                <Image source={{uri: item.image}} style={styles.image} resizeMode={'cover'} />
                <View style={styles.itemCount}>
                    <TouchableOpacity onPress={() => ShoppingList.removeItem(item)}>
                        <Text style={styles.itemCountButton}>{'-'}</Text>
                    </TouchableOpacity>
                    <Text>{counts[item.id]}</Text>
                    <TouchableOpacity onPress={() => ShoppingList.addItem(_.clone(item))}>
                        <Text style={styles.itemCountButton}>{'+'}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }

    const {items} = ShoppingList;
    // console.log(items);
    let data = resolveShoppingList(items);
    return (
        <Screen style={styles.container}>
            <StatusBar barStyle={'dark-content'}/>
            <View style={styles.tags}>
                <TouchableOpacity style={styles.tagButton(false)}>
                    <Text style={styles.tagButtonText(false)}>{"Costco"}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.tagButton(false)}>
                    <Text style={styles.tagButtonText(false)}>{"Target"}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.tagButton(true)}>
                    <Text style={styles.tagButtonText(true)}>{"Trader Joe's"}</Text>
                </TouchableOpacity>
            </View>
            <FlatList style={styles.list}
                      contentContainerStyle={styles.listContainer}
                      ListEmptyComponent={<View style={styles.empty}><Text>{'Shopping List Is Empty !'}</Text></View>}
                      data={data}
                      renderItem={renderItem}/>
            <TouchableOpacity style={styles.addItem}
                              onPress={addItem}>
                <Text style={styles.addItemText}>{'Add Item'}</Text>
            </TouchableOpacity>
        </Screen>
    );
});

function resolveShoppingList(rawList) {
    let uniqArray = _.uniqBy(rawList, 'id');
    return _.sortBy(uniqArray, 'name');
}

const accentColor = '#fc5e36';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f5f5f5',
        paddingHorizontal: 15,
        paddingBottom: 70
    },

    text: {
        fontSize: 25
    },

    list: {
        flex: 1,
        width: '100%'
    },

    listContainer: {
        flexGrow: 1,
        paddingTop: 15
    },

    empty: {
        height: 200,
        justifyContent: 'center',
        alignItems: 'center'
    },

    itemRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#dddddd'
    },

    tags: {
        marginTop: 15,
        flexDirection: 'row',
        maxHeight: 30,
        width: '100%'
    },

    tagButton: (selected: boolean) => {
        return {
            paddingHorizontal: 10,
            paddingVertical: 5,
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 5,
            borderWidth: 1,
            borderColor: accentColor,
            marginEnd: 10,
            backgroundColor: selected ? accentColor : '#ffffff',
        }
    },

    tagButtonText: (selected: boolean) => {
        return {
            color: selected ? '#ffffff' : accentColor
        }
    },

    itemCount: {
        flexDirection: 'row',
        width: 100,
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 15
    },

    itemCountButton: {
        fontSize: 30
    },

    image: {
        width: 50,
        height: 50,
        borderRadius: 10
    },

    addItem: {
        backgroundColor: accentColor,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        width: 200,
        height: 40
    },

    addItemText: {
        color: '#ffffff',
        fontWeight: 'bold'
    }
})
