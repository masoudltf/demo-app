import React from "react";
import {createStackNavigator} from '@react-navigation/stack';
import _ from 'lodash';
import {ShoppingListScreen} from "./ShoppingListScreen";
import {ProductsScreen} from "./ProductsScreen";
import {ProductScreen} from "./ProductScreen";

const Stack = createStackNavigator();

export class AddScreen extends React.Component {
    render() {
        return (
            <Stack.Navigator>
                <Stack.Screen name={'ShoppingListScreen'} component={ShoppingListScreen} options={{headerShown: false}}/>
                <Stack.Screen name={'ProductsScreen'} component={ProductsScreen} options={{title: 'Products'}}/>
                <Stack.Screen name={'ProductScreen'} component={ProductScreen} options={{title: 'Product Detail'}}/>
            </Stack.Navigator>
        );
    }
}
