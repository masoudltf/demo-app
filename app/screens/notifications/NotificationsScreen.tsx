import React from "react";
import {View, Text, StyleSheet} from "react-native";
import {Screen} from "../../components";

export class NotificationsScreen extends React.PureComponent {
    render() {
        return (
            <Screen statusBar={'dark-content'} style={styles.container}>
                <Text style={styles.text}>{'Notifications'}</Text>
            </Screen>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#eeeeee'
    },

    text: {
        fontSize: 25
    }
})
