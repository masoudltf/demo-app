import {ApiResponse, ApisauceInstance, create} from "apisauce"
import {getGeneralApiProblem} from "./api-problem"
import {ApiConfig, DEFAULT_API_CONFIG} from "./api-config"
import * as Types from "./api.types"
import _ from 'lodash';

/**
 * Manages all requests to the API.
 */
export class Api {
    /**
     * The underlying apisauce instance which performs the requests.
     */
    apisauce: ApisauceInstance

    /**
     * Configurable options.
     */
    config: ApiConfig

    /**
     * Creates the api.
     *
     * @param config The configuration to use.
     */
    constructor(config: ApiConfig = DEFAULT_API_CONFIG) {
        this.config = config
    }

    /**
     * Sets up the API.  This will be called during the bootup
     * sequence and will happen before the first React component
     * is mounted.
     *
     * Be as quick as possible in here.
     */
    setup() {
        // construct the apisauce instance
        this.apisauce = create({
            baseURL: this.config.url,
            timeout: this.config.timeout,
            headers: {
                Accept: "application/json",
            },
        })
    }

    async getProducts(): Promise<Types.GetProductsResult> {
        /*const response: ApiResponse<any> = await this.apisauce.get(`/users`)

        if (!response.ok) {
            const problem = getGeneralApiProblem(response)
            if (problem) return problem
        }*/

        try {
            return {kind: "ok", products: sampleProducts}
        } catch {
            return {kind: "bad-data"}
        }
    }

    async getProduct(id: string): Promise<Types.GetProductResult> {
        /*const response: ApiResponse<any> = await this.apisauce.get(`/product/${id}`)

        if (!response.ok) {
            const problem = getGeneralApiProblem(response)
            if (problem) return problem
        }*/

        try {
            /*const product: Types.Product = {
                id: response.data.id,
                name: response.data.name,
                price: response.data.price
            }*/
            let item = _.find(sampleProducts, {id: id});
            return {kind: "ok", product: item}
        } catch {
            return {kind: "bad-data"}
        }
    }
}

const sampleProducts = [
    {
        id: '1',
        name: 'Milk',
        price: 5,
        image: 'https://picsum.photos/id/1020/200/300'
    },
    {
        id: '2',
        name: 'Egg',
        price: 1,
        image: 'https://picsum.photos/id/102/200/300'
    },
    {
        id: '3',
        name: 'Mezzetta Organics Kalamata Olives',
        price: 20,
        image: 'https://picsum.photos/id/1019/200/300'
    },
    {
        id: '4',
        name: 'Kirkland Signatures Artichoke',
        price: 12,
        image: 'https://picsum.photos/id/1018/200/300'
    },
    {
        id: '5',
        name: 'Garaofalo Organics Spaghetti',
        price: 8,
        image: 'https://picsum.photos/id/1015/200/300'
    },
    {
        id: '6',
        name: 'Cucumber',
        price: 20,
        image: 'https://picsum.photos/id/1016/200/300'
    }
]
