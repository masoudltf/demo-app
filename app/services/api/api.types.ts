import {GeneralApiProblem} from "./api-problem"

export interface Product {
    id: string
    name: string,
    price: number
}

export type GetProductsResult = { kind: "ok"; products: Product[] } | GeneralApiProblem
export type GetProductResult = { kind: "ok"; product: Product } | GeneralApiProblem
