import React, {useState} from 'react';
import {StatusBar, StyleSheet, Image} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {AccountScreen, AddScreen, ChallengesScreen, ExploreScreen, NotificationsScreen} from './screens';

const tabIcons = {
    ExploreScreen: require('../assets/images/tabIcons/explore.png'),
    ChallengesScreen: require('../assets/images/tabIcons/challenge.png'),
    AddScreen: require('../assets/images/tabIcons/add.png'),
    NotificationsScreen: require('../assets/images/tabIcons/notifications.png'),
    AccountScreen: require('../assets/images/tabIcons/account.png')
}
const Tab = createBottomTabNavigator();
const accentColor = '#fc5e36';

function App() {

    const [exploreFocused, setExploreFocused] = useState(false);
    let inactiveColor = exploreFocused ? '#dddddd' : '#333333';

    return (
        <NavigationContainer>
            <StatusBar backgroundColor={'transparent'}
                       barStyle={'dark-content'}
                       animated={true}
                       hidden={false}
                       translucent={true}/>
            <Tab.Navigator initialRouteName={'AddScreen'}
                           screenOptions={({route}) => ({
                               tabBarIcon: ({focused}) => {
                                   let color = focused ? accentColor : inactiveColor;

                                   let extraStyles = {};

                                   if (route.name === 'AddScreen') {
                                       extraStyles.width = 50;
                                       extraStyles.height = 50;
                                   }

                                   return <Image source={tabIcons[route.name]}
                                                 style={[{width: 35, height: 35, tintColor: color}, extraStyles]}/>;
                               },
                           })}
                           sceneContainerStyle={{backgroundColor: '#fff'}}
                           tabBarOptions={{
                               activeTintColor: accentColor,
                               inactiveTintColor: inactiveColor,
                               style: styles.tabBar
                           }}>
                <Tab.Screen name="ExploreScreen"
                            listeners={{
                                focus: () => setExploreFocused(true),
                                blur: () => setExploreFocused(false),
                            }}
                            component={ExploreScreen}
                            options={{title: 'Explore'}}/>
                <Tab.Screen name="ChallengesScreen"
                            component={ChallengesScreen}
                            options={{title: 'Challenges'}}/>
                <Tab.Screen name="AddScreen"
                            component={AddScreen}
                            options={{title: ''}}/>
                <Tab.Screen name="NotificationsScreen"
                            component={NotificationsScreen}
                            options={{title: 'Notifications'}}/>
                <Tab.Screen name="AccountScreen"
                            component={AccountScreen}
                            options={{title: 'Account'}}/>
            </Tab.Navigator>
        </NavigationContainer>
    );
}

const styles = StyleSheet.create({
    tabBar: {
        position: 'absolute',
        backgroundColor: '#00000000',
        elevation: 0,
        borderTopWidth: 0
    }
})

export default App;
