import {types, destroy} from "mobx-state-tree"
import {ProductModel} from "..";

export const ShoppingList = types.model('ShoppingList')
    .props({
        items: types.array(ProductModel)
    })
    .actions(self => ({
        addItem(item){
            self.items.push(item)
        },
        removeItem(item) {
            destroy(item)
        }
    }))
    .create({items: []})
