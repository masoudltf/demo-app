import {types} from "mobx-state-tree"

export const ProductModel = types.model("Product")
    .props({
        id: types.string,
        name: types.maybe(types.string),
        price: types.maybe(types.number),
        image: types.maybe(types.string)
    })
